/*
Entrer ici les noms des images à classer.
Chaque nouveau tableau correspond à une nouvelle liste.
Pour créer une nouvelle liste, il suffit d'ajouter un nouveau tableau
sans oublier de mettre une vrigule à la fin de la ligne du tableau précédent.
Dans le tableau, les noms des images doivent être impérativement saisis dans
l'ordre de classement. Le script se chargera de les proposer dans un ordre aléatoire.
Les fichiers images doivent être placés dans le dossier img
*/
const items = {
  "renard-chasse": ["img1-1.jpg","img1-2.jpg","img1-3.jpg","img1-4.jpg","img1-5.jpg","img1-6.jpg","img1-7.jpg","img1-8.jpg"],
  "chiot-perdu": ["img2-1.jpg","img2-2.jpg","img2-3.jpg","img2-4.jpg","img2-5.jpg","img2-6.jpg","img2-7.jpg","img2-8.jpg"],
  "cache-cache": ["img3-1.jpg","img3-2.jpg","img3-3.jpg","img3-4.jpg","img3-5.jpg","img3-6.jpg","img3-7.jpg"],
  "les-courses": ["img4-1.jpg","img4-2.jpg","img4-3.jpg","img4-4.jpg","img4-5.jpg","img4-6.jpg","img4-7.jpg","img4-8.jpg"],
  "la-fusee": ["img5-1.jpg","img5-2.jpg","img5-3.jpg","img5-4.jpg","img5-5.jpg","img5-6.jpg","img5-7.jpg","img5-8.jpg"],
  "le-ski": ["img6-1.jpg","img6-2.jpg","img6-3.jpg","img6-4.jpg","img6-5.jpg","img6-6.jpg","img6-7.jpg","img6-8.jpg"],
  "chasse-aux-papillons": ["img7-1.jpg","img7-2.jpg","img7-3.jpg","img7-4.jpg","img7-5.jpg","img7-6.jpg","img7-7.jpg","img7-8.jpg"],
  "cheval-fougeux": ["img8-1.jpg","img8-2.jpg","img8-3.jpg","img8-4.jpg","img8-5.jpg","img8-6.jpg","img8-7.jpg","img8-8.jpg"],
  "pecheur-inattendu": ["img9-1.jpg","img9-2.jpg","img9-3.jpg","img9-4.jpg","img9-5.jpg","img9-6.jpg","img9-7.jpg","img9-8.jpg"],
  "le-train": ["img10-1.jpg","img10-2.jpg","img10-3.jpg","img10-4.jpg","img10-5.jpg","img10-6.jpg","img10-7.jpg","img10-8.jpg"],
  "la-randonnee": ["img11-1.jpg","img11-2.jpg","img11-3.jpg","img11-4.jpg","img11-5.jpg","img11-6.jpg","img11-7.jpg","img11-8.jpg"],
  "le-jongleur": ["img12-1.jpg","img12-2.jpg","img12-3.jpg","img12-4.jpg","img12-5.jpg","img12-6.jpg","img12-7.jpg","img12-8.jpg"],
  "depart-pour-la-mer": ["img13-1.jpg","img13-2.jpg","img13-3.jpg","img13-4.jpg","img13-5.jpg","img13-6.jpg","img13-7.jpg","img13-8.jpg"],
  "automne": ["img14-1.jpg","img14-2.jpg","img14-3.jpg","img14-4.jpg","img14-5.jpg","img14-6.jpg","img14-7.jpg","img14-8.jpg"],
};

/*
Indiquer dans ce tableau la hauteur des images des différentes séries,
la première valeur correspondant à la pemière série,
la seconde à la deuxième série, etc.
*/
const hauteurImages = ["145px","145px","145px","145px","145px","145px","145px","145px","145px","145px","145px","145px","145px","145px"];
