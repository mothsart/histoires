function alea(min,max) {
  var nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function melange(tableau) {
  var tirage;
  var tab_alea = [];

  for (var i = 0; i < tableau.length; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    } while (tableau[tirage] === 0);
    tab_alea[i] = tableau[tirage];
    tableau[tirage] = 0;
  }
  return tab_alea;
}
