"use strict"

const nbListes = Object.keys(items).length;
var histoireId;
var solution = [];
var listesFaites = [];
var titreHistoire;
var dernierSurvol;
// Espace minimal laissé en fin de ligne pour permettre le drop
var espaceDrop = 50;
const receptacleImg = document.getElementById("receptacle");

function getIntValeur(nbPixels) {
  let valeur;
  valeur = parseInt(nbPixels.substr(0, nbPixels.length - 2));
  return valeur;
}

function creeListeItems() {
  let i;
  let listeItems = items[histoireId];
  solution = items[histoireId].join();
  listeItems = melange(listeItems);
  for (i = 0; i < listeItems.length; i++) {
    let newImg = new Image();
    newImg.src = "img/" + listeItems[i];
    newImg.setAttribute('draggable', false);
    let container = document.createElement('span');
    container.className = "collection";
    container.id = "item" + i;
    container.setAttribute('draggable', true);
    container.addEventListener("dragstart", drag);
    container.addEventListener("dragend", finSurvol);
    container.appendChild(newImg);
    document.getElementById("reserve").appendChild(container);
  }
  receptacleImg.style.minHeight = hauteurImages[histoireId];
}

function changeDisableBtListe(value) {
    let _list = document.getElementsByClassName("btliste");
    for (var i = 0, len = _list.length; i < len; i++) {
        if (listesFaites.indexOf(_list[i].id) !== -1)
            _list[i].disabled = true
        else
            _list[i].disabled = value;
    }
}

function defListe(element) {
  reinitialise();
  histoireId = element.id;
  titreHistoire = element.innerHTML;
  changeDisableBtListe(true);
  document.getElementById("imprimer").disabled = true;
  commencer();
}

function getIdObjet(infosObjet) {
  if (!infosObjet)
    return null;
  let tableau = infosObjet.split("-");
  return tableau[0];
}

function getIdParent(infosObjet) {
  let tableau = infosObjet.split("-");
  return tableau[1];
}

function  reinitialise() {
  while (receptacleImg.firstChild) {
    receptacleImg.removeChild(receptacleImg.firstChild);
  }
}

function commencer() {
  creeListeItems();
  document.getElementById("verifier").disabled = false;
  document.getElementById('consignes').classList.add('hidden');
}


function afficheBravo() {
  document.getElementById("ecran-noir").className = "visible";
  document.getElementById("bravo").className = "visible";
}

function effaceBravo() {
  document.getElementById("ecran-noir").className = "invisible";
  document.getElementById("bravo").className = "invisible";
}

function verification() {
  let i;
  let proposition = "";
  let position;
  for (i = 0; i < receptacleImg.childNodes.length; i++) {
    position = receptacleImg.childNodes[i].childNodes[0].src.lastIndexOf("/") + 1;
    if (i + 1 < receptacleImg.childNodes.length) {
      proposition = proposition + receptacleImg.childNodes[i].childNodes[0].src.substr(position) + ",";
      }
      else {
        proposition = proposition + receptacleImg.childNodes[i].childNodes[0].src.substr(position);
      }
  }
  if (proposition !== solution) {
    showDialog("Malheureusement, toutes les images ne sont pas dans l'ordre !",0.5,'img/tux-triste.png', 115, 101, 'left');
    return;
  }
  document.getElementById('consignes').classList.remove('hidden');
  listesFaites.push(histoireId);
  if (listesFaites.length < nbListes) {
    afficheBravo();
  }
  else {
    showDialog("Félicitations ! <br/>Tu as réussi à classer correctement les 14 histoires.",0.5,'img/trophee.png', 128, 128, 'left')
  }
  document.getElementById("verifier").disabled = true;
  document.getElementById("imprimer").disabled = false;
  changeDisableBtListe(false);
  /* if (listesFaites.length == nbListes) {
    showDialog("Félicitations ! <br/>Tu as réussi à classer correctement les 14 histoires.",0.5,'img/trophee.png', 128, 128, 'left')
  } */
}

function resize() {
  // Si on dispose d'un espace suffisant pour le drop en fin de ligne, on supprime la ligne supplémentaire si elle existe
  if ((receptacleImg.lastChild) && (receptacleImg.lastChild.x + espaceDrop < receptacleImg.offsetWidth)) {
    if (getIntValeur(receptacleImg.style.minHeight) - getIntValeur(hauteurImages[numeroListe - 1]) > 0 ) {
      receptacleImg.style.minHeight = getIntValeur(receptacleImg.style.minHeight) - getIntValeur(hauteurImages[numeroListe - 1]) + "px";
    }
  }
  // Si l'espace en fin de ligne est insuffisant pour le drop, on ajoute une ligne
  if ((receptacleImg.lastChild) && (receptacleImg.lastChild.x + receptacleImg.lastChild.clientWidth + espaceDrop > receptacleImg.offsetWidth)) {
    receptacleImg.style.minHeight = getIntValeur(receptacleImg.style.minHeight) + getIntValeur(hauteurImages[numeroListe - 1]) + "px";
  }
}

function allowDrop(ev) {
    ev.preventDefault();
}

let stockage = null;
function drag(ev) {
	var infosObjet;
	infosObjet = ev.target.id + "-" + ev.target.parentNode.id;
  ev.dataTransfer.setData("text", infosObjet);
  stockage = infosObjet;
}

function auSurvol(ev) {
  ev.preventDefault();
  const idObjet  = getIdObjet(stockage);
  dernierSurvol = ev.target;
  if (!idObjet || ev.target.id === idObjet) {
    return;
  }
  let fake_id = 'fake-' + dernierSurvol.id;
  if (document.getElementById(fake_id))
    return;
  let fake_node = dernierSurvol.cloneNode(true);
  fake_node.id = fake_id;
  fake_node.classList.add('fake-element');
  dernierSurvol.after(fake_node);
  dernierSurvol.childNodes[0].classList.add('hidden');
}

function remove_drap_spaces() {
    let _list = document.getElementsByClassName("fake-element");
    for (var i = 0, len = _list.length; i < len; i++) {
        _list[i].parentNode.removeChild(_list[i]);
    }
    _list = receptacleImg.getElementsByClassName("hidden");
    for (var i = 0, len = _list.length; i < len; i++) {
        _list[i].classList.remove('hidden');
    }
}

function finSurvol(ev) {
    remove_drap_spaces();
}

function drop(ev) {
  const idObjet  = getIdObjet(ev.dataTransfer.getData('Text'));
  const idParent = getIdParent(ev.dataTransfer.getData('Text'));
  const Objet = document.getElementById(idObjet);
  ev.preventDefault();
  ev.target.classList.remove('collection--drag-over');
  if (idParent === "reserve") {
    Objet.addEventListener("dragover", auSurvol);
    Objet.addEventListener("dragleave", finSurvol);
  }
  if (ev.target.id === "receptacle") {
    ev.target.appendChild(Objet);
    }
    else {
      if (Objet.className === "collection") {
        ev.target.parentNode.insertBefore(Objet, ev.target);
      }
  }
  // Si l'espace en fin de ligne est insuffisant pour le drop, on ajoute une ligne
  if ((receptacleImg.lastChild) && (receptacleImg.lastChild.x + Objet.clientWidth + espaceDrop > receptacleImg.offsetWidth)) {
    receptacleImg.style.minHeight = getIntValeur(receptacleImg.style.minHeight) + getIntValeur(hauteurImages[numeroListe - 1]) + "px";
  }
  if (espaceDrop < parseInt(Objet.clientWidth / 2)) {
    espaceDrop = parseInt(Objet.clientWidth / 2);
  }
}

function pageImpression() {
  sessionStorage.setItem("titre",titreHistoire);
  sessionStorage.setItem("serie",listesFaites[listesFaites.length-1]-1);
  window.open('impression.html');
}

function load() {
    let hash = document.location.hash.substr(1);
    if (hash in items)
        defListe(document.getElementById(hash));
}

window.onresize = resize;
