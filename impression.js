function construitPageImpression() {
  let laDate = new Date();
  let liste = "";
  let zoneDate = document.getElementById('date');
  let Titre = document.getElementById('titre');
  let Images = document.getElementById('zone-images');

  // Récupère et affiche les données stockées localement
  zoneDate.innerHTML = "Date : " + laDate.getDate() +"/" + (laDate.getMonth()+1) +"/" + laDate.getFullYear();
  Titre.innerHTML = sessionStorage.getItem("titre");
  let Serie = sessionStorage.getItem("serie");
  let listeItems = items[Serie];
  for (var i = 0; i < listeItems.length; i++) {
    var newImg = new Image();
    newImg.src = "img/" + listeItems[i];
    Images.appendChild(newImg);
  }

  // Supprime les données stockées localement
  sessionStorage.removeItem("titre");
  sessionStorage.removeItem("titre");
}
window.onload = construitPageImpression();